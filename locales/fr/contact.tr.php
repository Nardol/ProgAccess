<?php $tr=array (
  '_' => 'contact',
  '_todo_level' => 0,
  '_last_author' => 'Corentin',
  '_last_modif' => 1621432748,
  'title' => 'À propos de {{site}}',
  'maintext' => '<p>Cette page a pour but de vous apporter quelques informations diverses et variées sur notre site, son équipe et autre...</p>

<h2>L\'administration de {{site}}</h2>
<p>Voici le détail des membres qui composent l\'équipe d\'administration du site&nbsp;:</p>
<ul>
	{{teamlist}}
</ul>
<ul>
	<li>Dessinateur du logo du site&nbsp;: <a href="https://facebook.com/crayongra">David ENGÉLIBERT</a>.</li>
	<li><a href="/contacter.php">Contactez-nous</a>.</li>
</ul>
<ul>
<li><a href="https://discord.gg/nd45eqN">Rejoignez-nous sur Discord</a></li>
	<li><a href="skype:live:miklhcos?chat">Discuter sur Skype (nécessite qu\'un client Skype soit installé)</a></li>
	<li><a href="skype:live:miklhcos?call">Nous appeler sur Skype (nécessite qu\'un client Skype soit installé)</a></li>
</ul>

<h2>Horaires du site</h2>
<h3>Tâches automatisées</h3>
<p>Plusieurs tâches sont exécutées de manière automatique sur le site, les voici&nbsp;:</p>
<ul>
	<li>21:23&nbsp;: actualisation des diapositives de l\'accueil&nbsp;;</li>
	<li>21:28&nbsp;: exécution des tâches liées aux comptes membres&nbsp;;</li>
	<!-- <li>21:33&nbsp;: publication du message listant les mises à jour de la journée écoulée sur Facebook&nbsp;;</li> -->
	<li>Aléatoirement entre 20:12 et 20:21&nbsp;: envoi éventuel de la lettre d\'informations aux abonnés.</li>
</ul>

<h2>Histoire du site</h2>
<p>Ce site est d\'abord né le 25/05/2015 sous le nom <q>Accessibilité Programmes</q>.<br />
Ce même site renaîtra sous le nom <q>ProgAccess33</q> le 19/09/2016, et enfin, le nom sera de nouveau raccourci pour devenir <q>ProgAccess</q> lors de la V15.0 le 09/09/2017.<br />
Ce site vient de l\'envie de Corentin (un étudiant non-voyant) de partager sa passion avec un grand public et surtout de rassembler en un seul endroit les logiciels accessibles aux déficients visuels et bien sur à tous.</p>
<p>Durant l\'été 2018, plusieurs changements majeurs ont eu lieu&nbsp;: en août, <a href="opensource.php">le code source</a> a été publié sous licence <a href="https://gnu.org">GNU AGPL</a>, le site devient donc un logiciel libre. Plus tard dans le même mois, l\'équipe entreprend la traduction du site, commençant par l\'italien, l\'espéranto, l\'anglais et l\'espagnol. Michel Such nous confie le site <a href="https://nvda-fr.org">NVDA-FR</a>, et un site dédié à <a href="https://accessikey.nvda-fr.org">l\'AccessiKey</a> est créé.</p>

<h2>Suivre l\'actu et contribuer</h2>
<h3>Sur les réseaux sociaux</h3>
<ul>
	<li><a href="https://twitter.com/ProgAccess">@ProgAccess sur Twitter</a></li>
	<li><a href="https://www.facebook.com/ProgAccess">Page ProgAccess sur Facebook</a></li>
	<li><a href="https://framasphere.org/people/f5342a7058e60136500c2a0000053625">Page ProgAccess sur Diaspora*</a></li>
</ul>

<h3>En interne sur le site</h3>
<ul>
	<li><a href="/newsletter.php">S\'inscrire à la lettre d\'infos (envoi chaque soir entre 20:12 et 20:21)</a></li>
	<li><a href="/journal_modif.php">Journal des modifs (actualisation en temps réel)</a></li>
	<li><a href="/journal_modif.xml">Flux RSS (actualisation en temps réel)</a></li>
	<li><a href="/api">Pour les développeurs&nbsp;: les données ouvertes via notre API</a></li>
</ul>

<h3>Version de {{site}}</h3>
<p>{{site}} est régulièrement mis à jour dès que d\'importants changements de code sont réalisés. La version actuelle de site est la {{lastv}}, elle porte l\'identifiant {{lastvid}} et a été publiée le {{lastvdate}}, vous pouvez <a href="/u?id={{lastvu}}">en consulter les changements ici</a>.</p>

<h3>Code source de {{site}}</h3>
<p>{{site}} est libre et open-source, son code est disponible sur <a href="https://gitlab.com/ProgAccess/ProgAccess">GitLab</a>. Vous pouvez <a href="opensource.php">en savoir plus sur le développement ici</a>.</p>

<h2>Nos autres sites</h2>
<p>Notre équipe travaille également activement sur d\'autres sites dont voici la liste&nbsp;:</p>
<ul>
	<li><a href="https://www.nvda-fr.org">NVDA-FR</a></li>
	<li><a href="https://www.zettascript.org">ZettaScript</a></li>
</ul>

<h2>Nous soutenir</h2>
<a href="https://liberapay.com/ProgAccess/donate" style="border: 2px solid #f6c915; border-radius: 5px; color: #1a171b; background: white; display: inline-block; font-family: Helvetica Neue,Helvetica,sans-serif; font-size: 14px; max-width: 150px; min-width: 110px; position: relative; text-align: center; text-decoration: none;">
	<span style="background-color: #f6c915; display: block; font-family: Ubuntu,Arial,sans-serif; font-style: italic; font-weight: 700; padding: 3px 7px 5px;">
		<img src="https://liberapay.com/assets/liberapay/icon-v2_black.svg?etag=.ImgxnJdB0KMMAVqul9G6Uw~~" height="20" width="20" style="vertical-align: middle;" alt="Logo Liberapay"/>
		<span style="vertical-align: middle;">LiberaPay</span>
	</span>
	<span style="display: block; padding: 5px 15px 2px;"><span style="color: #f6c915; position: absolute; left: -2px;">&#10132;</span>Nous recevons <br><span style="font-size: 125%">0,00&#8239;€</span><br> par semaine</span>
</a>
<p>Vous pouvez faire un don en <a href="https://monnaie-libre.fr">monnaie libre</a> à ProgAccess, <a href="https://demo.cesium.app/#/app/wot/EEGevmgQcgzXou2ucaf1S9pCMvwKfu56ukRRLPn4D3y9/">à la clé EEGevmgQ</a>.</p>

<h2>Traductions françaises de logiciels et applications</h2>
<p>Corentin est à l\'origine de nombreuses traductions françaises de logiciels ou applications, en voici la liste.</p>
<table>
<caption>Applis et logiciels traduits</caption>
<thead>
<tr>
<th>Application ou logiciel</th>
<th>Plateforme</th>
</tr>
</thead>
<tbody>
<tr>
<td>TeamTalk (participe également activement à l\'amélioration globale du programme)</td>
<td>Windows, Android, IOS, Linux et Mac</td>
</tr>
<tr>
<td>Sullivan+</td>
<td>Android</td>
</tr>
<tr>
<td>TWBlue</td>
<td>Windows</td>
</tr>
<tr>
<td>NVDA (cotraducteur)</td>
<td>Windows</td>
</tr>
</tbody>
</table>',
  'teamlist_item' => '{{age}} ans, dans l\'équipe depuis le {{date}}.',
); ?>
