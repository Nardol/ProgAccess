<?php $tr=array (
  '_' => 'accueil',
  '_todo_level' => 0,
  '_last_author' => 'Pascal',
  '_last_modif' => 1621417424,
  'title' => '{{site}}&nbsp;: {{slogan}}',
  'happynewyear' => 'Toute l\'équipe {{site}} vous souhaite une excellente année {{year}}&#8239;!',
  'texttitle' => '{{slogan}}&#8239;!',
  'maintext' => '<p>Pour empêcher qu\'Internet soit une ressource exclusive et privée, luttons pour le rendre plus ouvert et accessible.</p>
<h3>Accessible</h3>
<p>Une chose accessible (quelqu\'elle soit) est utilisable partout, par tous, tout le temps (dans la mesure du possible).
Internet (ou l\'informatique en général) est un bien commun, œuvre intellectuelle et matérielle de l\'Humanité, qui doit être accessible à tous. Cela implique qu\'il doit être aussi bien utilisable avec ou sans handicap, quelque soit le budget, la géographie, la taille de l\'écran, les performances de l\'ordinateur, etc.</p>
<p>L\'accessibilité d\'un logiciel ou d\'un matériel est déterminée par le prix, mais surtout par des choix techniques à tous les niveaux. À nous de faire les bons choix, de développer des outils appropriés (logiciels, études, normes), et de l\'imposer aux éditeurs et aux fabricants.</p>

<h3>Ouvert</h3>
<p>Quand on dit qu\'une chose est ouverte, ça veut dire que cette chose n\'est pas une boîte noire dont l\'utilisateur ne pourrait qu\'appuyer sur les boutons extérieurs, préalablement décidés par quelqu\'un d\'autre dont on ne connaît pas les intentions. Ces boîtes noires, ce sont principalement l\'œuvre des géants du web, les GAFAM et compagnie (Google, Apple, Facebook, Amazon, Microsoft), qui développent parallèlement leurs technologies dans le plus grand secret. Mais nous avons une solution&nbsp;: l\'Open-Source est une garantie pour l\'utilisateur de savoir exactement ce que fait un programme sur son ordinateur. Cette pratique consiste à publier le code source (la recette) d\'un programme informatique. Tout le monde est alors libre d\'étudier son fonctionnement, de le modifier et de l\'améliorer. Ces libertés &mdash; d\'utiliser, d\'étudier, de partager et de modifier &mdash; sont garanties par les licences libres. (<a href="https://gnu.org">Plus d\'infos ici</a>)</p>
<p>Vous voulez une bonne nouvelle&#8239;? Eh bien en contribuant à rendre le Web ouvert, on peut contribuer à le rendre accessible, car si le code d\'un projet est public et libre alors n\'importe qui peut l\'améliorer, même au niveau accessibilité.</p>

<h3>Les objectifs de {{site}}</h3>
<p>Lors de son lancement initial en 2015, ce site luttait pour l\'accessibilité avec ce seul moyen&nbsp;: répertorier les logiciels sous Windows utilisables par les déficients visuels.<br />
Aujourd\'hui, la communauté active du site a changé, l\'équipe s\'est agrandie, et nous sommes conscients que ce seul moyen n\'est pas suffisant.<br />
Nous avons donc conservé l\'activité initiale, mais avons agrandi notre champ d\'action&nbsp;:</p>
<ul>
<li>Répertorier de manière intuitive et organisée les logiciels utilisables par tous sous Windows, Android ou GNU/Linux&#8239;;</li>
<li>Proposer des tutoriels (audio ou écrits) ainsi que, dans le futur, un service d\'assistance informatique à distance pour permettre aux utilisateurs les moins avancés de se débrouiller le mieux possible avec leur matériel&#8239;;</li>
<li>Promouvoir l\'échange et la participation par le biais du <a href="https://forum.progaccess.net">forum</a>&#8239;;</li>
<li>Créer et défendre un maximum le logiciel libre (ou la culture libre en général)&#8239;;</li>
<li>Faire évoluer les mentalités et faire prendre conscience de l\'extrême importance de l\'accessibilité dans tous les domaines (informatique, santé, commerce, loisirs et bien d\'autre)&#8239;;</li>
<li>Proposer des tests de matériel plus ou moins spécialisé.</li>
</ul>
<p>Pour réaliser tout cela, une équipe essentiellement composée de jeunes travaille pour coder et maintenir le site, vous pouvez <a href="/contact.php">la découvrir ici</a>.<br />
Nous nous appuyons également sur vous tous sans qui le site n\'existerait pas aujourd\'hui.</p>
<p>Un bug à remonter, un message à faire passer à l\'administration du site&#8239;? <a href="/contacter.php">Contactez-nous</a>.</p>

<h2>Comment aider&nbsp;?</h2>
<p>Vous êtes convaincu, ce site vous plaît et vous voulez l\'aider&#8239;? Suivez le guide&nbsp;:</p>
<ul>
<li>Proposez votre aide pour coder le site, le traduire, mettre à jour des logiciels, proposer des tutoriels et autres via le <a href="/contacter.php">formulaire de contact</a>.</li>
<li>Faites connaître le site pour l\'aider à se développer, notamment en le partageant sur les réseaux sociaux.</li>
<li>Si vous avez des compétences en programmation web ou PHP, allez voir <a href="/opensource.php">le code source du site</a> et améliorez-le&#8239;! Il est publié sous licence GNU AGPL v3.</li>
</ul>

<h2>On parle de nous&#8239;!</h2>
<p>Ils nous ont fait l\'honneur de publier des articles sur {{site}}&nbsp;:</p>
<ul>
<li><a href="https://sospc.name/sospc-aime-ProgAccess33-net/">"SOSPC aime&nbsp;: ProgAccess33.net"</a></li>
<li><a href="https://korben.info/ressources-non-mal-voyants.html">Korben&nbsp;: "Des ressources pour les non / mal voyants"</a></li>
<li><a href="http://forums.cnetfrance.fr/topic/1367199-ProgAccess33--un-site-dedie-aux-non-voyants-qui-facilite-l-acces-aux-logiciels/">CNet&nbsp;: "ProgAccess33&nbsp;: un site dédié aux non-voyants qui facilite l\'accès aux logiciels"</a></li>
</ul>

<h2>Site aux normes</h2>
<p>Parce que l\'accessibilité passe surtout par des aspects techniques, le respect de ces normes en est une garantie&nbsp;:</p>
<ul>
<li>CSS 3.0</li>
<li>WCAG 2.0 (Level AA)</li>
<li>HTML 5.1</li>
</ul>',
  'sliderinactext' => '<p>Vous avez choisis de ne pas utiliser notre système d\'informations défilantes, retrouvez donc ci-dessous nos dernières actualités.</p>',
  'sliderinactitle' => 'Actualités',
  'mailconfirmtext' => 'Nous vous remercions pour votre message. Vous devriez recevoir bientôt un e-mail de réponse.',
); ?>
