<?php $tr=array (
  '_' => 'newsletter',
  '_todo_level' => 0,
  '_last_author' => 'Pascal',
  '_last_modif' => 1550315894,
  'mail_expire_title' => '{{site}} : votre abonnement à la lettre d\'informations expire bientôt',
  'mail_expire_text' => 'Bonjour {{mail}},

Votre abonnement à la lettre d\'informations de {{site}} expire le {{date}}.
Cliquez sur le lien suivant si vous souhaitez renouveler votre abonnement :
https://www.progaccess33.net/nlmod.php?id={{hash}}

Cordialement,
L\'administration {{site}}',
  'email_from' => 'L\'administration {{site}}',
  'mail_title' => 'Lettre d\'informations {{site}}',
  'mail_logo' => 'Logo',
  'mail_hello' => 'Bonjour {{mail}}',
  'mail_editlink' => 'Cliquez ici pour modifier votre abonnement, le renouveler ou vous désinscrire.',
  'mail_noreply' => 'Merci de ne pas répondre, ceci est un mail automatique.',
  'mail_signature' => 'Cordialement.<br />L\'administration {{site}}',
); ?>
