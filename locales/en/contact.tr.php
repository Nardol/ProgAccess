<?php $tr=array (
  '_' => 'contact',
  '_todo_level' => 2,
  '_last_author' => 'Corentin',
  '_last_modif' => 1618399433,
  'title' => 'About {{site}}',
  'maintext' => '<p>Here are the website\'s team, history, contacts... (all dates and times are given in the French timezone, GMT+1 or +2)</p>

<h2>The {{site}} team</h2>
<p>Here is the list of the website administration team members:</p>
<ul>
	{{teamlist}}
</ul>
<ul>
	<li>The drawer of our logos: <a href="https://facebook.com/crayongra">David ENGÉLIBERT</a>.</li>
	<li><a href="/contacter.php">Contact us</a>.</li>
</ul>
<ul>
	<li><a href="skype:live:miklhcos?chat">Discuss on Skype (needs a Skype client to be installed)</a></li>
	<li><a href="skype:live:miklhcos?call">Call us on Skype (needs a Skype client to be installed)</a></li>
</ul>

<h2>Website\'s timetable</h2>
<h3>Automated tasks</h3>
<p>Several tasks are automatically executed on the website:</p>
<ul>
	<li>21:23&nbsp: refreshing the home page\'s slides;</li>
	<li>21:28&nbsp;: member accounts related tasks;</li>
	<li>Bitween 20:12 and 20:21&nbsp;: sending newsletters.</li>
</ul>

<h2>Website\'s history</h2>
<p>This website was born on 2015-05-25 named <q>Accessibilité Programmes</q>.<br />
It has been renamed <q>ProgAccess33</q> on 2016-09-19, and finally, its name has been shortened again to became <q>ProgAccess</q> with the V15.0 on 2017-09-09.<br />
This website comes from the Corentin\'s desire (a blind high school student) to share his passion and to gather in one place the softwares accessible by blind people and by everybody.</p>
<p>During summer 2018, many changes came: in August, <a href="opensource.php">the source code</a> has been published under <a href="https://gnu.org">GNU AGPL</a> license, so the website is a free (libre) software. Later in the same month, the team start translating the site, beginning with Italian, Esperanto, English and Spanish. Michel Such gave the website <a href="https://nvda-fr.org">NVDA-FR</a> to us, and a dedicated website is created for <a href="https://accessikey.nvda-fr.org">the AccessiKey</a>.</p>

<h2>Follow us</h2>
<h3>On social networks</h3>
<ul>
	<li><a href="https://twitter.com/ProgAccess">@ProgAccess on Twitter</a></li>
	<li><a href="https://www.facebook.com/ProgAccess">ProgAccess on Facebook</a></li>
	<li><a href="https://framasphere.org/people/f5342a7058e60136500c2a0000053625">ProgAccess on Diaspora*</a></li>
</ul>

<h3>Directly on the site</h3>
<ul>
	<li><a href="/newsletter.php">Subscribe to the newsletter (sent each night bitween 20:12 and 20:21)</a></li>
	<li><a href="/journal_modif.php">Modifications journal (real-time)</a></li>
	<li><a href="/journal_modif.xml">RSS stream (real-time)</a></li>
	<li><a href="/api">For developpers: open-data via our API</a></li>
</ul>

<h2>Our other websites</h2>
<p>We are also actively working on other websites:</p>
<ul>
	<li><a href="https://www.nvda-fr.org">NVDA-FR</a> (Corentin and Pascal)</li>
	<li><a href="https://www.zettascript.org">ZettaScript</a> (Corentin, Adrien and Pascal)</li>
</ul>

<h2>Help us</h2>
You can donate in <a href="https://monnaie-libre.fr">libre currency</a> to ProgAccess, <a href="https://demo.cesium.app/#/app/wot/EEGevmgQcgzXou2ucaf1S9pCMvwKfu56ukRRLPn4D3y9/">with the public key EEGevmgQ</a>. (<a href="https://duniter.org/en/duniter-why-how/">What is a libre currency?</a>)',
  'teamlist_item' => '{{age}} years old, joined on {{date}}.',
); ?>
