<?php
/* Copyright (c) 2017 Pascal Engélibert
This file is part of PHPSocialClient.
PHPSocialClient is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
PHPSocialClient is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public License along with PHPSocialClient. If not, see <http://www.gnu.org/licenses/>.
*/
$tr=array('PHP_LANG'=>'fr_FR.UTF8','HTTP_LANG'=>'fr,fr-FR','PHP_TIME_HOUR'=>'%H:%M:%S','PHP_TIME_DAY'=>'%a %e à %H:%M:%S','PHP_TIME_MONTH'=>'%a %e %b à %H:%M:%S','PHP_TIME_YEAR'=>'%a %e %b %Y à %H:%M:%S','FB_TITLE'=>'{{title}} | Facebook','FB_SHARE'=>'Partager','FB_FBLNK'=>'Commenter ou aimer','TW_TITLE'=>'{{title}} | Twitter','TW_LIKE'=>'J\'aime','TW_LIKE_ARIA'=>'J\'aime','TW_SHARE'=>'Partager le Tweet','TW_SHARE_ARIA'=>'Partager','TW_TWLNK'=>'Voir le statut','TW_TWLNK_ARIA'=>'Voir le statut'); ?>
